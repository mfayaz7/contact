import Vue from 'vue';
import Router from 'vue-router';
import DashboardHome from "./views/DashboardHome";
import * as auth from './services/auth_service'

Vue.use(Router);

const routes = [
    {
        path: '/home',
        name: 'dashboardhome',
        component: DashboardHome,
        children:[
            {
                path: '',
                name: 'dashboard',
                component: () =>import('./views/admin/Dashboard.vue'),
                meta: {
                    requiresAuth: true,
                    role : 'administrator'
                }
            },
            {
                path: '/user',
                name: 'user',
                component: () =>import('./views/admin/User.vue')
            },
            {
                path: '/roles',
                name: 'roles',
                component: () =>import('./views/admin/Role.vue')
            },
            {
                path: '/contact',
                name: 'Contact',
                component: () =>import('./views/admin/Contact.vue')
            },
        ],
        beforeEnter(to, from, next){
            if (!auth.isLoggedIn()){
                next('/login')
            }
            else {
                next();
            }
        }
    },
    {
        path: '/register',
        name: 'register',
        component: () =>import('./views/auth/Register.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: () =>import('./views/auth/Login.vue'),
        beforeEnter(to, from, next){
            if (!auth.isLoggedIn()){
                next();
            }else if (auth.getRole() === 'administrator'){
                next('/home');
            }else {
                next();
            }
        }
    },
    {
        path: '/',
        name: 'home',
        component: () =>import('./views/Home.vue'),
        // beforeEnter(to, from, next){
        //     if (!auth.isLoggedIn()){
        //         next();
        //     }else if (auth.getRole() === 'administrator'){
        //         next('/home');
        //     }else {
        //         next();
        //     }
        // }
    },
    {
        path: '/reset-password',
        name: 'reset-password',
        component: () =>import('./views/auth/ResetPassword.vue')
    }
];

const router = new Router({
    mode: 'history',
    routes:routes,
    linkActiveClass:'active'
});
export default router;
