import {http} from './http_service'

export function createContact(data) {
    return http().post('/contacts',data);
}

export function deleteContact(id) {
    return http().delete(`/contacts/${id}`);
}

export function updateContact(id,data) {
    return http().post(`/contacts/${id}`,data);
}

export function getContacts() {
    return http().get('/contacts');
}
