<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $contacts = Contact::query()->orderBy('id','DESC')->paginate(10);

        return response()->json( $contacts,200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|unique:users|email|max:255',
            'message' => 'required|string|max:255|min:6',
        ]);


        $contact = Contact::create([
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message,
            ]);

//        Mail::to(config('access.email'))->send(new ContactMail());
        return response()->json( $contact,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Contact $contact, Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|unique:users|email|max:255',
            'message' => 'required|string|max:255|min:6',
        ]);


       $contact->name=$request->name;
       $contact->email=$request->email;
       $contact->message=$request->message;
       $contact->save();

        return response()->json( $contact,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Contact $contact)
    {
        if($contact->delete()){
            return response()->json([
                'message' => 'Contact delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
