

## Features

- Laravel 8
- Vue + VueRouter + Vuex
- Pages with dynamic import and custom layouts
- Login, register, email verification and password reset
- Passport Authentication
- Bootstrap 4 + Font Awesome 5



## Installation

- `composer create-project --prefer-dist devgene/Laravel-Vue-Boilerplate`
- Edit `.env` and set your database connection details
- (When installed via git clone or download, run `composer update` and `php artisan key:generate` )
- `php artisan migrate`
- `npm install`
- `php artisan passport:install`
- `php artisan serve`

## Usage

#### Development

```bash
# Build and watch
npm run watch

```

#### Production

```bash
npm run production
```

#### credential
```bash
username: admin@gmail.com
password: 123456
```
